class GRAPH{
		//takes an x and y size for the graph, an x and y margin that will appear around the graph on the canvas, data is the form of a 2D array, size of the ciricles that appear on the points.
		//the ID of the canvas html tag to draw on, the ID of the div tag to output the hover over data to, the colour of the axis in a #hex and the color of the line in a #hex.
		constructor(x, y, xMargin, yMargin, data, circleSize, canvasID, outPutId, axisColor, lineColor){
			this.c = document.getElementById(canvasID);
			this.display = document.getElementById(outPutId);
			this.ctx = this.c.getContext("2d");
			this.GRAPH_X_CONSTANT = x;
			this.GRAPH_Y_CONSTANT = y;
			this.X_MARGIN = xMargin;
			this.Y_MARGIN = yMargin;
			this.CIRCLE_SIZE = circleSize;
			this.c.width = this.GRAPH_X_CONSTANT + this.X_MARGIN;
			this.c.height = this.GRAPH_Y_CONSTANT + this.Y_MARGIN;
			this.dataSet = [];
			this.pointLocations = [];
			this.tempSet = data;
			this.mouseover = false;
			this.widthRatio;
			this.hightRatio;
			this.AXIS_COLOR = axisColor;
			this.LINE_COLOR = lineColor;
			this.buildAxis();
			this.yAxisStart = [10, 10];
			this.yAxisEnd = [10, this.GRAPH_Y_CONSTANT];
			this.xAxisEnd = [this.GRAPH_X_CONSTANT, this.GRAPH_Y_CONSTANT];
			this.xAxisStart = this.yAxisEnd;
			this.yStart = this.yAxisEnd[1];
			this.xStart = this.xAxisStart[0];
			this.buildPointlocations();
			this.drawAxis();
			this.drawData();
			this.numbPoints = this.pointLocations.length;
			this.createListener();
		}
	
		createListener(){
			var self = this;
			this.c.addEventListener('mousemove', function(event){
				var x = event.offsetX;
				var y = event.offsetY;
				
				var numPoints = self.numbPoints;
				self.c.style.cursor = "auto";
				for(var q = 0; q < numPoints; q++){
					if((x > self.pointLocations[q][0] - self.CIRCLE_SIZE && x < self.pointLocations[q][0] + self.CIRCLE_SIZE) && (y > self.pointLocations[q][1] - self.CIRCLE_SIZE && y < self.pointLocations[q][1] + self.CIRCLE_SIZE)){
						self.c.style.cursor = "pointer";
						self.display.innerHTML = "On graph: x = " + self.dataSet[q][0] + ", y = " + self.dataSet[q][1] + "<br> On canvas: x = " + self.pointLocations[q][0] + ", y = " + self.pointLocations[q][1] + "<br> Raw data: x = " + self.tempSet[q][0] + ", y = " + self.tempSet[q][1];
					}
				}
			}, false);
		}
	
		drawline(sX, sY, eX, eY, color){
			this.ctx.beginPath();
			this.ctx.moveTo(sX, sY);
			this.ctx.lineTo(eX, eY);
			this.ctx.strokeStyle = color;
			this.ctx.stroke();
		}
	
		buildAxis(){
			var noData = this.tempSet.length;
			var currhigh = 0;
			var currWidth = 0;
			for(var x = 0; x < noData; x++){
				if(this.tempSet[x][1] > currhigh){
					currhigh = this.tempSet[x][1];
				}
				if(this.tempSet[x][0] > currWidth){
					currWidth = this.tempSet[x][0];
				}
			}
			
			var hightRatio = (this.GRAPH_Y_CONSTANT - this.Y_MARGIN) / currhigh;
			var widthRatio = (this.GRAPH_X_CONSTANT - this.X_MARGIN) / currWidth ;
			
			for(var x = 0; x < this.tempSet.length; x++){
				var newH = this.tempSet[x][1] * hightRatio;
				var newW = this.tempSet[x][0] * widthRatio;
				this.dataSet.push([newW, newH]);
			}
		}

		drawAxis(){
			this.drawline(this.yAxisStart[0], this.yAxisStart[1], this.yAxisEnd[0], this.yAxisEnd[1], this.AXIS_COLOR);
			this.drawline(this.xAxisStart[0], this.xAxisStart[1], this.xAxisEnd[0], this.xAxisEnd[1], this.AXIS_COLOR);
		}
	
		buildPointlocations(){
			var setLength = this.dataSet.length - 1;
			for(var x = 0; x < setLength; x++){
				this.xlinestart = this.xStart + this.dataSet[x][0];
				this.ylinestart = this.yStart - this.dataSet[x][1];
				this.xlineend = this.xStart + this.dataSet[x + 1][0];
				this.ylineend = this.yStart - this.dataSet[x + 1][1];
				this.pointLocations.push([this.xlinestart, this.ylinestart]);
			}
		}
	
		drawData(){
			var setLength = this.dataSet.length - 1;
			for(var x = 0; x < setLength; x++){
			
				var xlinestart = this.xStart + this.dataSet[x][0];
				var ylinestart = this.yStart - this.dataSet[x][1];
				var xlineend = this.xStart + this.dataSet[x + 1][0];
				var ylineend = this.yStart - this.dataSet[x + 1][1];
				
				var circle = new Path2D();
				circle.arc(xlinestart,ylinestart , this.CIRCLE_SIZE, 0, 2 * Math.PI);
					
				this.ctx.fillStyle = this.LINE_COLOR;
				this.ctx.fill(circle);
				
				this.drawline(xlinestart, ylinestart, xlineend, ylineend, this.LINE_COLOR);
			}
			
			this.xPoint = this.xStart + this.dataSet[this.dataSet.length - 1][0];
			this.yPoint = this.yStart - this.dataSet[this.dataSet.length - 1][1];
				
			var circle = new Path2D();
			circle.arc(this.xPoint, this.yPoint, this.CIRCLE_SIZE, 0, 2 * Math.PI);
					
			this.ctx.fillStyle = this.LINE_COLOR;
			this.ctx.fill(circle);
			this.pointLocations.push([this.xPoint, this.yPoint]);
		}
	}